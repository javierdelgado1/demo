# vuely

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your end-to-end tests
```
npm run test:e2e
```


### run and deploy
git add . && git commit -m "updating token key api " && git push && git push heroku master && heroku logs --tail