// Sidebar Routers
export const menus = {
	'message.general': [
		{		
			title: 'message.dashboard',
			action: 'zmdi-view-dashboard',
			active: true,
			exact: true,
			items: null,
			path: '/dashboard',
			label:'Old',
			code: "BD"	
		},
	],
	'message.managment': [
		{
			action: 'zmdi-balance',
			title: 'message.banks',
			exact: true,
			path: '/banks',
			active: false,
			label: 'Old',
			items: null,
			code: "BB"
		},
		{
			action: 'zmdi-accounts',
			title: 'message.users',
			exact: true,
			path: '/users',
			active: false,
			label: 'Old',
			items: null,
			code: "BU"
		},
		{
			action: 'zmdi-accounts-list-alt',
			title: 'message.clients',
			exact: true,
			path: '/clients',
			active: false,
			label: 'Old',
			items: null,
			code: "BLC"
		},
		{
			action: 'zmdi-link',
			title: 'message.localbitcoinAccounts',
			exact: true,
			path: '/accounts',
			active: false,
			label: 'Old',
			items: null,
			code: "BLA"
		},
		{
			action: 'zmdi-assignment',
			title: 'message.tradeSetup',
			exact: false,
			active: false,
			label: 'Old',
			items: [
				{ title: 'message.tradeStatus', exact: true, path: '/tradeStatus', label: 'Old', code: "BTSTA" },
				{ title: 'message.tradeType', exact: true, path: '/tradeType', label: 'Old', code: "BTTY" },
			],
			code: "BTSET"
		}
	],
	'message.localbitcoin': [
		{
			action: 'zmdi-lock',
			title: 'message.conciliation',
			exact: true,
			path: '/conciliation',
			active: false,
			label: 'Old',
			items: null,
			code: "BCON"
		},
		{
			action: 'zmdi-card',
			title: 'message.payments',
			exact: true,
			path: '/payments',
			active: false,
			label: 'Old',
			items: null,
			code: "BPAY"
		},
		{
			action: 'zmdi-collection-text',
			title: 'message.trades',
			exact: true,
			path: '/trades',
			active: false,
			label: 'Old',
			items: null,
			code: "BT",
		},
		{
			action: 'zmdi-collection-bookmark',
			title: 'message.closing',
			exact: true,
			path: '/closing',
			active: false,
			label: 'Old',
			items: null,
			code: "BCON"
		},
	],
	'message.bot': [
		{
			action: 'zmdi-flash',
			title: 'message.bot',
			exact: true,
			path: '/bot',
			active: false,
			label: 'Old',
			items: null,
			code: "BBOT"
		},
		{
			action: 'zmdi-memory',
			title: 'message.botSetup',
			exact: true,
			path: '/botSetup',
			active: false,
			label: 'Old',
			items: null,
			code: "BBOTSET"
		},
	]
}
