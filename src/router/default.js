import Full from 'Container/Full'

const Dashboard = () => import('Views/dashboard/Dashboard');

//Banks
const Bank = () => import('Views/banks/Bank');

//Users
const UsersList = () => import('Views/users/UsersList');

//Accounts
const AccountLocal = () => import('Views/account_local/AccountLocal');

//Payments
const RegisterPayment = () => import('Views/payments/RegisterPayment');

//Trades
const Trade = () => import('Views/trades/Trade');
const TradeStatus = () => import('Views/trades/TradeStatus');

//Trade type
const TradeType = () => import('Views/trades/TradeType');

const Conciliation = () => import('Views/conciliation/Conciliation');

const Closing = () => import('Views/closing/Closing');

const Clients = () => import('Views/clients/Clients');

//Bot
const Bot = () => import('Views/bot/Bot');
const BotSetup = () => import('Views/bot/BotSetup');


export default {
  path: '/',
  component: Full,
  redirect: '/dashboard',
  children: [{
      path: '/dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
        title: 'message.dashboard',
        breadcrumb: null
      }
    },
    {
      path: '/banks',
      component: Bank,
      meta: {
        requiresAuth: true,
        title: 'message.banks',
        breadcrumb: 'Banks'
      }
    },
    {
      path: '/users',
      component: UsersList,
      meta: {
        requiresAuth: true,
        title: 'message.users',
        breadcrumb: 'Users'
      }
    }, 
    {
      path: '/accounts',
      component: AccountLocal,
      meta: {
        requiresAuth: true,
        title: 'message.localbitcoinAccounts',
        breadcrumb: 'Localbitcoin Accounts'
      }
    },
    {
      path: '/payments/',
      component: RegisterPayment,
      meta: {
        requiresAuth: true,
        title: 'message.payments',
        breadcrumb: 'message.payments'
      }
    },
    {
      path: '/trades',
      component: Trade,
      meta: {
        requiresAuth: true,
        title: 'message.trades',
        breadcrumb: 'message.trades'
      }
    },
    {
      path: '/tradeType',
      component: TradeType,
      meta: {
        requiresAuth: true,
        title: 'message.tradeType',
        breadcrumb: 'message.tradeType'
      }
    },
    {
      path: '/tradeStatus',
      component: TradeStatus,
      meta: {
        requiresAuth: true,
        title: 'message.tradeStatus',
        breadcrumb: 'message.tradeStatus'
      }
    },
    {
      path: '/conciliation',
      component: Conciliation,
      meta: {
        requiresAuth: true,
        title: 'message.conciliation',
        breadcrumb: 'message.conciliation'
      }
    },
    {
      path: '/closing',
      component: Closing,
      meta: {
        requiresAuth: true,
        title: 'message.closing',
        breadcrumb: 'message.closing'
      }
    },
    {
      path: '/clients',
      component: Clients,
      meta: {
        requiresAuth: true,
        title: 'message.clients',
        breadcrumb: 'message.clients'
      }
    },
    {
      path: '/bot',
      component: Bot,
      meta: {
        requiresAuth: true,
        title: 'message.bot',
        breadcrumb: 'message.bot'
      }
    },
    {
      path: '/botSetup',
      component: BotSetup,
      meta: {
        requiresAuth: true,
        title: 'message.botSetup',
        breadcrumb: 'message.botSetup'
      }
    },
  ]
}