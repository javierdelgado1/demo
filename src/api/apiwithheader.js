import axios from "axios";
import api from ".";

export default {
  data() {
    return {
      baseUrlApi: false
        ? "http://localbitcoinapimeerkat.test"
        : "http://10.0.100.45:8000/api/v1/",
      baseUrl: false
        ? "http://localbitcoinapimeerkat.test"
        : "http://10.0.100.45:8000",
      user: {
        name: "",
        email: "",
        role: ""
      }
    };
  },
  created() {
    //console.log(process.env);
    var user = JSON.parse(localStorage.getItem("user"));
    if (user != "" && user != null) {
      this.user.name = user.user.name;
      this.user.email = user.user.email;
      this.user.role = user.user.roles;
    }
  },
  methods: {
    apiConexion() {
      return axios.create({
        baseURL: this.baseURL + "/"
      });
    },
    api(isfile = null) {
      var user = JSON.parse(localStorage.getItem("user"));
      if (user != "" && user != null) {
        var access_token = user.token.access_token;

        if (isfile != null) {
          return axios.create({
            baseURL: this.baseUrlApi,
            headers: {
              Accept: "application/json",
              Authorization: "Bearer " + access_token,
              "Content-Type": "multipart/form-data"
            }
          });
        }
        return axios.create({
          baseURL: this.baseUrlApi,
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + access_token
          }
        });
      }
      return axios.create({
        baseURL: baseUrlApi
      });
    }
  }
};
